class CharacterApi {

  getData() {
    //check to see if it already has local storage
    if (!this.getLocalStorageData()) {
      return this.getCharacterData().then(data => {
        this.setLocalStorageData(data);
        return data;
      });
    }
    else {
      return Promise.resolve(this.getLocalStorageData());
    }
  }

  getLocalStorageData(dataToGet) {
    return JSON.parse(localStorage.getItem('results'));
  }

  setLocalStorageData(dataToSet) {
    return localStorage.setItem('results', JSON.stringify(dataToSet));
  }

  getImages() {
    var images = require('./data/character-data.json');
    return Promise.resolve(images);
  }

  getCharacterData() {
    var promises = [this.getImages(), this.getDataFromApi()]
    return Promise.all(promises)
      .catch(error => console.error(error))
      .then(data => {
        //const images = [], const characters = []
        const [images, characters] = data;
        //merge them - compare a value in each and return as a bundle
        characters.results.forEach(element => {
          images.forEach(i => {
            if (i.name === element.name) {
              //image, description
              element.image = i.image;
              element.description = i.description;
            }
          })
        });
        return characters;
      })
  }

  getDataFromApi() {
    return fetch('https://swapi.co/api/people', {
      method: 'GET',
    })
      .then(response => {
        return response.json();
      })
      .catch((error => console.error(error)))
  }
}

export default new CharacterApi();