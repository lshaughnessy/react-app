import React from 'react';

import './Loader.css';

function Loader() {
  return (
    <div className="loader-container">
      <div className="loader"></div>
      <div className="loader-txt">Loading Data...</div>
    </div>
  );
}

export default Loader;