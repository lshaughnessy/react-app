import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Characters extends Component {

  //check this component is called with a 'click handler' function attached
  static propTypes = { clickHandler: PropTypes.func.isRequired };

  render() {
    return (
      <div className="primary-btn" onClick={e => this.props.clickHandler(e)}>
        <img src={this.props.image} alt={this.props.name} />
      </div>
    );
  }
}
export default Characters;