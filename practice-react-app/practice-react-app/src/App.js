import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import { Modal, Carousel } from 'react-bootstrap';

import Characters from './components/Characters'
import './components/Characters.css';

import Loader from './components/Loader';
import CharacterApi from './CharacterApi'

class App extends Component {

  //components can have state by setting this.state in the constructor
  constructor(props) {
    super(props);

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.toggleCharacter = this.toggleCharacter.bind(this);

    this.state = {
      isModalOpen: false,
      trackModal: null,
      index: 0,
      direction: null
    }
  }

  componentDidMount() {
    CharacterApi.getData()
      .then(data => this.setState({ results: data.results }))
      .catch((error => {
        console.error(error);
      }))
  }


  //Modal functionality
  openModal(index) {
    this.setState({ isModalOpen: true, trackModal: index });
  }

  closeModal() {
    this.setState({ isModalOpen: false, trackModal: null });
  }

  //Carousel Functionality
  toggleCharacter(selectedIndex, e) {
    this.setState({
      trackModal: selectedIndex,
      direction: e.direction
    });
  }


  render() {
    const { results } = this.state;
    if (results) {
      return (
        <div className="App">
          <h1>React App</h1>
          <div className="character-grid">
            {results.map((character, i) =>
              <Characters clickHandler={() => this.openModal(i)} key={character.id} name={character.name} image={character.image} description={character.description} gender={character.gender} birth_year={character.birth_year} />
            )}
          </div>
          <Modal className="character-modal" show={this.state.isModalOpen} onHide={this.closeModal}>
            <Modal.Header closeButton></Modal.Header>
            <Modal.Body>
              <Carousel activeIndex={this.state.trackModal} direction={this.state.direction} onSelect={this.toggleCharacter}>
                {results.map((character, i) =>
                  <Carousel.Item key={character.name}>
                    <img className="modal-image" src={character.image} alt={character.name} />
                    <Carousel.Caption>
                      <h1>{character.name}</h1>
                      <h4>{character.description}</h4>
                      <h5><strong>Gender:</strong> {character.gender}</h5>
                      <h5><strong>Birthday:</strong> {character.birth_year}</h5>
                    </Carousel.Caption>
                  </Carousel.Item>
                )}
              </Carousel>
            </Modal.Body>
          </ Modal>
          <div className="footer"><p>Lexi Shaughnessy - {(new Date()).getFullYear()}</p></div>
        </div>
      );
    }
    else {
      return (
        <Loader />
      )
    }
  }
}

export default App;